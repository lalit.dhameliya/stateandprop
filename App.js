import React, { Component } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';

class Blink extends Component {

  componentDidMount() {
    // Toggle the state every second
    setInterval(() => (
      this.setState(previousState => (
        { isShowingText: !previousState.isShowingText }
      ))
    ), 1000);
  }

  //state object
  state = { isShowingText: true };

  render() {
    if (!this.state.isShowingText) {
      return null;
    }

    return (
      <Text>{this.props.text}</Text>
    );
  }
}

export default class BlinkApp extends Component {
  render() {
    let pic = {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    };
    return (
      <View>
        <View style={{ height: 300 }}>
          <Text style={styles.titleBanner}>
            State
        </Text>
          <Blink text='I love to blink' />
          <Blink text='Yes blinking is so great' />
          <Blink text='Why did they ever take this out of HTML' />
          <Blink text='Look at me look at me look at me' />
        </View>
        <View>
          <Text style={styles.titleBanner}>
            props
          </Text>
          <Image source={pic} style={{ alignSelf:'center',width:300,height:200 }} />
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  titleBanner: {
    alignSelf: 'stretch',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 28,
    backgroundColor: '#000000',
    color: '#ffffff',
    padding: 5,
    marginBottom: 16
  }
});
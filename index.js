/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import BlinkApp from './App';

AppRegistry.registerComponent(appName, () => BlinkApp);
